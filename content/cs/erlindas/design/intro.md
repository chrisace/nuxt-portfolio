---
title: The Overall Design
tech:
  - Photoshop
  - Illustrator
---

To provide an online presence they wanted, we decided on a simple, yet airy design. This design was based on a light theme, with the colors of green and yellow as featured in their logo, as well as purple as an additional complimentary color.
