### Gotcha's

For this project, I learned that if given the luxury of creative freedom on both the design and build side, there are many benefits. Traditionally when doing web development, the web design should literally be fully completed out by the designer and approved by the client. However, on this project, I was able to make the design and also make changes and improvements while I was building out the design in code! This benefited me because it eliminated the need to go back to the "designer" or design phase, and it also showed that I can "design in code" as necessary.

### Customer Satisfaction

Erlinda's was satisfied with the overall branding and visual design of their website. Over a short period of time, they informed me that they were no longer frequently being referred to by the previous restaurant's name, but by "Erlinda's". This was the ultimate goal, and I'm happy to have helped them achieve this. They even asked me to make some flyers and lawn signs for them!
