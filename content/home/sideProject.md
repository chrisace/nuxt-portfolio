---
title: Everlasting FC
url: https://everlastingfc.now.sh
image: /images/work/everlastingfc.jpg
type: Side Project
---

Lately, I've been all-in on Vue via Gridsome. The goal of this project was to create a fun way for me to learn and experience Gridsome, GraphQL, and utilize an external API.

The content for this site is based from a group that im a part of in a video game called Final Fantasy XIV. The site uses the [XIVAPI](https://xivapi.com/) to pull data about my group and each character in the group.

For more information, checkout the [source code](https://gitlab.com/chrisace/ffxivapi-gridsome)!