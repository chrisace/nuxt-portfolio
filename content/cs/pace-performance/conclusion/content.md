### Increased & Steady Success

Over a period of 3 months, we noticed that their conversion rates grew steadiily and overall reaction to the new website was positive. I was nervous about such a drastic change from their signature red color branding, to a blue color theme, but it actually wasn't much of a detriment! The owner and their sales reps also found it easier to navigate their website in order to help customers find products they needed on their website.

### Learning Experiences

On a personal note, I did learn a lot from this project. I learned that though I have design and website site experience, owners will generally have a better understanding of their audience and how to cater to them. Their views may not always align with a designer's view, and I learned that this is perfectly fine! As long as we can reach the goals of a project, then all will be fine.

Additionally, I learned that in a team environment, is may be beneficial to ask for help earlier, rather than later. Sometimes one just has to swallow their pride and think about how to accomplish the end-goal as efficiently as possible. The end-result of the collaboration may even help the overall team as well.
