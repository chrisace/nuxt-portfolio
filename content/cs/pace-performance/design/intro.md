---
title: The Redesign
tech:
  - Photoshop
---

For the redesign, Pace primarily wanted to accomplish two things:
The first being a "rebranding" from their original red color, to a blue color. The second being a streamlined homepage with emphasis on search and shopping experience.
