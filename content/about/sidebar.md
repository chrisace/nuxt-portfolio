## TL;DR

If you just want a list of what I have experience with, here it is:

### Code

- Semantic HTML
- CSS/SCSS from scratch
- Javascript/Vue/jQuery
- Laravel Blade

---

### Design & UI/UX

- Photoshop
- Illustrator
- Adobe XD
- Figma

---

### Other

- Jira
- Git/GitLab
- Windows, Mac OS
- eCommerce design
- Cross Browser Testing

---

### Links

<a rel="noreferrer noopener nofollow" href="https://gitlab.com/chrisace" target="_blank" class="c2a__btn c2a__btn--about">GitLab</a> <a rel="noreferrer noopener nofollow" href="https://www.linkedin.com/in/christian-ferrer-84185134/" target="_blank" class="c2a__btn c2a__btn--about">LinkedIn</a>