---
title: Site Build
tech:
  - HTML
  - CSS
  - Smarty
  - jQuery
  - Bootstrap
wsmIntro: true
---

### Overall Build

This build was not too bad, overall. I found that after completing many site builds at WSM, a good portion of my time was spent on dropdown and mega menus. Since this site did not utilize any dropdown menus, my build time was not as intensive.

### Struggles

One of my struggles on this site was getting the client to provide the content necessary to complete the site. This was actually a common problem on other projects, but a lack of content definitely slows down a build process.

In order to help with this, I had to have daily check-ins with the client. Additionally, I had to work with my project manager to make sure that they felt some "pressure" that we would not meet the expected deadlines if they did not provide the content we needed.

Although no bad relationships in this project, it was definitely a pain point. However, Paramount ultimately provided all the content we needed for a successful site launch!

### CSS

On this project, I also started to use more of the latest additions of CSS like CSS custom properties, CSS Grid, and CSS Flexbox. I saw the opportunity to level up my skills while providing Paramount the layouts they desired. I also made sure that this was inline with the browser support we had to maintain, and I got the OK.
