---
title: Pace Performance
projectUrl: https://paceperformance.com/
logoImage: /images/work/case-studies/pace-performance/logo-blue.png
logo: true
wireframes: false
styleGuide: true
graphics: true
siteDesign: true
siteBuild: true
casePrimaryColor: "#148cc3"
---

Pace Performance is a big company that specializes in GM Performance products. For this project, the goal was to do complete redesign of their site which included more streamlined user experience for users to find products they need specific vehicles. By doing so, they hoped to achieve a cleaner website with a friendlier user experience to help increase conversion.
