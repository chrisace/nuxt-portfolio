---
title: Tailored UX And UI
images:
  - /images/work/case-studies/bedslide/ux-2.jpg
  - /images/work/case-studies/bedslide/ux-1.jpg
---

Providing the User Experience Bedslide desired for their products was key to a successful design. They envisioned that after a user selects the Year Make and Model of their vehicle, they would be presented with the appropriate Beslide SKU, along with any associated products or accessories.

Additionally, Bedslide wanted the search results to display results in separate sections, rather than products all clumped in one area.

While I was able to successfully design these user experiences for them, WSM only natively supported some of these UX ideas. In the next section, I'll go into more detail of the roadblocks I hit.
