## The Beginning

### It All Started On NeoPets.

My first taste of web design and development started when I was in middle school around the year 2000-2001. It was in an online game site called [neopets.com](http://neopets.com). Within the game, you could join "guilds", which is similar to a club. Each respective guild was given a custom, HTML-enabled page to represent the guild.

<img src="/images/placeholder.png" data-src="/images/about/JirachiGLS.gif" alt="Sample Guild" class="lazy abt-beg__img" />

<small>(This isn't mine -- just an example of how the guild pages looked. Wish I still had mine though!)</small>

People had some awesome-looking (at the time) guild pages! This intrigued me so much that I started to research to try and deck out my own guild page.

### The Trends

At the time, the trend was colorful/eye-bleeding backgrounds and/or tiled patterns. Comic Sans was the king of fonts, with Impact being a close second. Animated gifs, scrolling text, and TyPiNg LiKe THiS would all get you cool points too.

**LiTtLe DiD i kNoW tHaT i'D bE HoOkEd!**

I remember that [lissaexplains.com](http://lissaexplains.com) was the go-to for learning all the cool HTML Tables and Frame techniques, and I spent hours on that site just trying to grasp the concepts to build cool stuff.

### From Then On

From that point onward, this became one of my hobbies and all I wanted to design and code amazing layouts. I started to dabble more and found myself designing and building random web pages on angelfire/geocities. I also started finding myself getting more interested in other digital mediums. The door was opened.

---

## Life After Neopets

There was so many different digital avenues that I ventured after the whole neopets and angelfire/geocities phase. This was a great time to grow up as a kid.

### Forum Signatures

I got more into graphic design and found myself joining online graphic design forums. I would enter "signature of the week" competitions to see which user could design the best forum signature. A forum signature was simply a graphic that appears at the end of each of your forum posts. This was the start of me learning graphic design software such as Paint Shop Pro and Adobe Photoshop.

<img class="lazy abt-aft__sig abt-aft__sig--1" src="/images/placeholder.png" data-src="/images/about/leehyorisig.png" alt="Sig" /> <img class="lazy abt-aft__sig abt-aft__sig--2" src="/images/placeholder.png" data-src="/images/about/overwhelmed2.png" alt="Sig" /> <img class="lazy abt-aft__sig abt-aft__sig--3" src="/images/placeholder.png" data-src="/images/about/linktexturedcopy.jpg" alt="Sig" /> <img class="lazy abt-aft__sig abt-aft__sig--5" src="/images/placeholder.png" data-src="/images/about/kkreuksig.png" alt="Sig" />

### Video Editing

I also ended up diving into video editing in high school. I liked to make short music videos that were synced well with the music. I usually edited video game or anime content. I didn't make anything too crazy at the time, I just liked working and being creative in Windows Movie Maker, hah!

<div class="yt yt--4by3">
  <iframe
    class="yt__vid lazy"
    width="560"
    height="315"
    src="/images/placeholder.png"
    data-src="https://www.youtube.com/embed/97Tnlh6SWtk"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  ></iframe>
</div>
<p><small>(Wow! I can't believe this video is 13 years old).</small></p>

### Web Layouts

Around the same time, I got into designing and coding layouts for various blogs and even myspace. This was a good time!

<img class="lazy abt-aft__layout" src="/images/placeholder.png" data-src="/images/about/blog.png" alt="crappy blog layout" />
  
### More And More
To add to the list, I took a liking to photography, PC building/gaming, and even windows desktop customization. The list goes on, but I think I should cut it here.

---

## As Of Late

### College

I graduated from San Diego State University with a degree in Art:Multimedia. I was surrounded by many digital mediums that I was definitely interested in. Mediums such as Typography, Graphic Design, Photography, Video Editing, 3d animation, and web design and development. From my time here, I always felt the best connection towards web development, so that's what I pursued.

### In The Present

Fast-forward to today, I've toned it down and mainly focus on web design/development. While the other digital mediums still catch my interest, there's just an overwhelming amount to learn. Hell, even web itself is always changing and it's hard to keep up, but that's what keeps it fun and interesting!

---

## Closing

<p class="lead">Woo! You made it! Just a little more to go.</p>

In terms of my experience and skill set in web, I would say that I'm proficient in HTML CSS, and presentational JS. Lately, I've been trying to strengthen my core Javascript knowledge as well as dive into the world of JS frameworks. Vue JS has been what I've been attracted to so far. Additionally, the JAMstack has also caught my attention and I love the whole concept of static sites (just like the old days!).

I'm now looking to expand my horizons and take the next step in my career path. Ideally, I'd like to join a digital creative agency or a tech company where I can work and collaborate in a team of like-minded individuals. I am a team-oriented guy and love to collaborate with others. I feel like this is the best way to code because it allows for ideas to be combined and it allows for better perspective on the coding tasks at hand.

---
