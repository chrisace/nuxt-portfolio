---
title: Site Build
tech:
  - HTML
  - SCSS
  - Vanilla JS
  - Gulp
  - Git
---

Since this was simply a one-pager, I decided to build this site as simply as possible. This meant no JS or CSS frameworks, but rather leverage good ol' HTML, CSS, and vanilla JS if necessary.

Additionally, since I had creative freedom on this site, I tweaked the original web design a bit at my discretion to provide the best UX and aesthetics as I built the site. This is one thing I end up finding myself doing more and more as I build sites. To me, this is a major benefit of being both the designer and the coder. So if you're wondering why the site doesn't look exactly as it does on the web design, it's because of intentional changes on the build phase.

To cater to more customers on-the-go, the main goal for this build was to make sure the page loaded fast and performed well on mobile devices. Many of their visitors would be on their mobile device and would want to check to see what time the store was open and what they had to offer at a quick glance. Therefore, I used the built in Chrome audits to make sure their light house scores were in the 90s.

On this project, I implemented the images using the element in an effor to use the .webp image format that google always recommends. This was definitely a file-size improvement over the standard .jpg and .png files.

In terms of build process, I utilized a simple Gulp process to make sure the CSS and JS files were combined and minified as necessary.
