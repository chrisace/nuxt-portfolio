### Capturing The Bedslide Experience

It was great experience to extend the WSM platform from just being an eCommerce store. Adjusting the product pages to use life style images, creating Bedslide Stories, and creating a more designed-based, rather than shopping based experience on the homepage was all part of the vision, and I was glad to able to bring this to life.

### JQuery/Javascript Gains

As mentioned in the Site Build section, with WSM being a closed platform, some of the user experiences that Bedslide wanted required heavy DOM manipulation to achieve. From working on this project, I've gained more skills in critical thinking in order to achieve desired user experiences within the scope of DOM manipulation.
