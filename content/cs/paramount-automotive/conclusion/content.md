### Challenging My Comfort Zone

Throughout my career as designer/developer, I was not always comfortable with client interaction. It was even more intimidating (but also humbling) when big company projects would be assigned to me and would place their trust in my designs. As time passed in my career, I grew more confident and comfortable with these client interactions, but that's not to say that I was completely out of the water.

Maybe it was more of a pride thing, but I had trouble saying "no" to clients that requested things outside the scope of the project. I had this work ethic in me of always trying to deliver to the best of my ability. However, with the guidance of my higher ups, I then learned that if a client wants something that was not agreed on in the contract, I should feel comfortable in saying that it was not part of the contract, but we could quote that for them to get that work complete. This is exactly what I did for this project.

In doing so, we solved multiple issues that provided a win-win situation. First off, we as a company, get paid for the extra work we're doing. Two, we're able to document this extra work in change-order for future reference, so that we could keep track of extra items that clients are asking for and possibly create packages for these additional lines of work. Lastly, would respect the fact that we are also a business and though we entertain additional work, we have to quote.

Generally, clients have been happy to pay more as long as the quote made sense for them. If any quotes are denied, then it saves us time and resources to work on other things. Ultimately, these quotes usually resulted in a win-win situation.

### Improving My Toolbelt

I have to say that I've always wanted variables in vanilla CSS. I understand that variables are available in SCSS, but SCSS was not something that could be used within WSM. Being able to use CSS variables was awesome and it's something that I've continued to use as more projects came my way.

The releases of CSS grid along with flexbox were also very welcome additions. Since WSM was primarily a 12-column bootstrap grid platform, there were some layouts where that grid-system was quite limiting. CSS grid definitely helped solve some issues that would have taken a lot more to do using traditional floats and bootstrap grids. I've been using grid and flexbox more and more.
