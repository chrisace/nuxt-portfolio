export default {
  mode: "universal",
  server: {
    port: 8000, // default: 3000
    host: "0.0.0.0" // default: localhost
  },
  target: "static", // default: 'server'
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: "en"
    },
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "robots", content: "noindex" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "icon", type: "image/png", href: "/favicon.png" }
      // { rel: 'stylesheet', href: './css/global.css' }
    ],
    script: [],
    __dangerouslyDisableSanitizers: ["script"]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: "#fff",
    color: "teal",
    height: "5px"
  },
  /*
   ** Global CSS
   */
  css: ["@/assets/css/global.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // { src: "@/plugins/prism", ssr: false },
    // '~/plugins/prism'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    "@nuxt/components",
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-97937255-1"
      }
    ]
  ],
  components: true,
  /*
   ** Nuxt.js modules
   */
  modules: ["nuxt-webfontloader", "@nuxt/content"],
  content: {
    markdown: {
      externalLinks: {},
      basePlugins: [
        "remark-squeeze-paragraphs",
        "remark-slug",
        "remark-autolink-headings",
        "remark-external-links",
        "remark-footnotes"
      ],
      plugins: [],
      prism: {
        theme: "prismjs/themes/prism-tomorrow.css"
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    // friendlyErrors: false,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  webfontloader: {
    custom: {
      families: ["Work Sans:n3,n4,n7", "Montserrat:n3,n4,n6"],
      urls: [
        // for each Google Fonts add url + options you want
        // here add font-display option
        "https://fonts.googleapis.com/css?family=Work+Sans:300,400,700&display=swap",
        "https://fonts.googleapis.com/css?family=Montserrat:300,400,600&display=swap"
      ]
    }
    // google: {
    //   //Quicksand:400,700|Work+Sans:300,400,700
    //   // families: ['Quicksand:400,700','Work+Sans:300,400,700']
    //   families: ["Work+Sans:300,400,700", "Montserrat:300,400,600"]
    // }
  },
  layoutTransition: {
    name: "layout",
    mode: "out-in"
  }
};
