export const homeHeroData = {
  data() {
    return {
      logoName: "Christian Ferrer",
      logoSlogan: "Front-end Web Developer & Web Designer"
    };
  }
};

export const homeAboutData = {
  data() {
    return {
      aboutInfo: {
        aboutTitle: "About",
        aboutLead: "A Vue on Me",
        aboutMsg:
          "Heya! My name is Christian and I've been on the web dev train since 2012. It really all started when I was just a kid in middle school. Lately I've been interested in the JAMSTACK and Vue JS.",
        aboutBtn: "Get to Know Me",
        aboutSkls: [
          "Semantic HTML",
          "CSS/SCSS from scratch",
          "Javascript/Vue JS",
          "Photoshop/Figma",
          "Accessibility"
        ],

        resumeTitle: "Quick Resume",
        resume: [
          {
            work: "Graphic Language",
            exp: "2020 - Present -- Front-end Developer"
          },
          {
            work: "Web Shop Manager",
            exp: "2012-2020 -- Front-end Developer & Design Lead"
          },
          {
            work: "San Diego State University",
            exp: "2011 Graduate -- Bachelors in Multimedia"
          }
        ],
        resumeDownload:
          "https://drive.google.com/open?id=1p9z1sqB7dAqmj64GFTDgPgRJDrGo9ikQ",
        resumeBtn: "Download Full Resume"
      }
    };
  }
};

export const homeWorkData = {
  data() {
    return {
      workInfo: {
        workTitle: "Featured Work",
        workLead: "Sites Designed and Built",
        workMsg:
          "Featured here are a few sites that I have both designed and built. The case studies go into more detail about the design and development process. ",
        workBtn: "More Work",
        workHero: {
          name: "Bedslide",
          url: "bedslide.com",
          image: "/images/work/bedslide@2x.jpg",
          pUrl: "/work/bedslide",
          type: "Automotive eCommerce"
        },
        workFeatured: [
          {
            name: "Bedslide",
            url: "bedslide.com",
            image: "/images/work/bedslide@2x.jpg",
            pUrl: "/work/bedslide",
            logo: "/images/work/case-studies/bedslide/bedslide_logo.svg",
            type: "Automotive eCommerce"
          },
          {
            name: "Paramount Automotive",
            url: "paramount-automotive.com",
            image: "/images/work/paramountautomotive@2x.jpg",
            pUrl: "/work/paramount-automotive",
            logo:
              "/images/work/case-studies/paramount-automotive/new-logo-white.png",
            type: "Automotive eCommerce"
          },
          {
            name: "Pace Performance",
            url: "paceperformance.com",
            image: "/images/work/paceperformance@2x.jpg",
            pUrl: "/work/pace-performance",
            logo: "/images/work/case-studies/pace-performance/logo-blue.png",
            type: "Automotive eCommerce"
          },
          {
            name: "Erlindas",
            url: "erlindas.com",
            image: "/images/work/erlindas@2x.jpg",
            pUrl: "/work/erlindas",
            logo: "/images/work/case-studies/erlindas/erlindas.svg",
            type: "Restaurant"
          }
        ]
      }
    };
  }
};
