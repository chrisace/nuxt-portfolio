---
title: Bedslide
projectUrl: https://bedslide.com/
logoImage: /images/work/case-studies/bedslide/bedslide_logo.svg
linkSite: false
sourceUrl: null
logo: false
wireframes: true
styleGuide: true
graphics: true
siteDesign: true
siteBuild: true
casePrimaryColor: "#990000"
---

Bedslide is a well-known brand that provides a unique storage mechanism for truck beds. As a change from their previous site, Bedslide's goal for this project was to have a light, but bold website that promoted their products and associated with them outdoors lifestyle. This would allow the products to be portrayed as an ideal companion for those who love to be outdoors.
