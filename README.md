# Portfolio

[View Portfolio](https://chrfer.com/)

## Intro

Portfolio to showcase all the work I've done. I built this in Nuxt in order to familiarize myself with it and go all-in in the Vue ecosystem.

## Goals

- Create an online portfolio for myself to showcase my web work.
- Familiarize myself with Nuxt and the Vue Ecosystem

## Tech Used

- Vue / Javascript
- HTML
- CSS
- Figma for mockups

## Services Used

- [GitLab](https://gitlab.com/) -- Git Repo
- [Netlify](https://netlify.com/) -- Static File Hosting and deployment
