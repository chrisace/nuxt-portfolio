---
title: Erlinda's Filipino Cuisine
projectUrl: https://erlindas.com/
logoImage: /images/work/case-studies/erlindas/erlindas.svg
sourceUrl: https://gitlab.com/chrisace/erlindas_v2
logo: true
wireframes: true
styleGuide: true
graphics: true
siteDesign: true
siteBuild: true
casePrimaryColor: "#009444"
---

At the time, Erlinda's was a brand new restaurant that was taking over an existing restaurant. They needed help in establishing their own identity with a unique and refreshing logo. Additionally, they needed a website to give themselves an online presence and to distinguish themselves as a separate restaurant from the previous establishment. I was given creative freedom to help them achieve these goals.
