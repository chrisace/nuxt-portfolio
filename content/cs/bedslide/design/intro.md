---
title: A Fresh Design
tech:
  - Photoshop
  - Moqups
---

While this site was to be built with eCommerce, it was not the main purpose for the site. The vision they provided for this site was to promote the Bedslide brand, provide the necessary information for each product, and showcase the lifestyle that comes with these products.
