export const sideProject = {
  data() {
    return {
      currentSideProject: {
        image: "/images/work/everlastingfc.jpg",
        url: "https://everlastingfc.now.sh",
        btnText: "View Everlasting FC",
        name: "Everlasting FC",
        type: "Side Project",
        msg: `<p>Lately, I've been all-in on Vue via Gridsome. The goal of this project was to create a fun way for me to learn and experience Gridsome, GraphQL, and utilize an external API.</p><p>The content for this site is based from a group that im a part of in a video game called Final Fantasy XIV. The site uses the <a href="https://xivapi.com/" target="_blank" rel="noreferrer">XIVAPI</a>  to pull data about my group and each character in the group.</p></p><p>For more information, checkout the <a href="https://gitlab.com/chrisace/ffxivapi-gridsome" target="_blank">source code</a>!</p>`
      }
    };
  }
};
export const moreWorkData = {
  data() {
    return {
      workGallery: [
        {
          name: "hef4yrs",
          url: "https://wedding.happilyeverferrer.com/",
          type: "wedding",
          image: "/images/work/hef4yrs.jpg"
        },
        {
          name: "Everlasting FC",
          url: "https://everlastingfc.now.sh/",
          type: "gaming",
          image: "/images/work/everlastingfc.jpg"
        },
        {
          name: "Kerma TDi",
          url: "https://kermatdi.com/",
          type: "automotive eCommerce",
          image: "/images/work/kermatdi@2x.jpg"
        },
        {
          name: "Pickup For Trucks",
          url: "https://pickupfortrucks.com/",
          type: "automotive eCommerce",
          image: "/images/work/pickupfortrucks.jpg"
        },
        {
          name: "Moonlite Diesel",
          url: "https://moonlitediesel.com",
          type: "automotive eCommerce",
          image: "/images/work/moonlitediesel@2x.jpg"
        },
        {
          name: "Happily Ever Ferrer",
          url: "https://happilyeverferrer.com",
          type: "engagement",
          image: "/images/work/hef@2x.jpg"
        }
      ]
    };
  }
};
