---
title: Site Build
tech:
  - HTML
  - CSS
  - Smarty
  - jQuery
  - Bootstrap
images:
  - /images/work/case-studies/bedslide/build-ux-tabs.jpg
  - /images/work/case-studies/bedslide/build-ux-results.jpg
wsmIntro: true
---

### Overall Build

The build for this project was challenging, but I was able to rise to the occasion and had a fun time building this one. The main blockers I had were regarding certain user experiences that were not native to the WSM platform.

### UX & DOM Manipulation

In order to craft all of the UX that bedslide wanted, some heavy DOM manipulation was needed in order to achieve this within the desired WSM areas.

### Additional Products Tabs

On a product fitment page, WSM does not natively display any related products after a vehicle has been chosen. In order to achieve this user experience, I first had to work with our development team to have them provide the additional products data onto this page through a JS object. Once I had this data, I would be able to create the required additional products tabs through some DOM manipulation.

Once the JS object was available to me, I had to manipulate the data to the way I needed it. This had several requirements, but to summarize, I had to:

- Gather the specific data I needed from the object. These items included the additional product's title, price, product link, product type, and product image url
- Create the proper HTML wrappers in JS to append the mentioned data into
- Append the new HTML data into bootstrap tab HTML containers
- Create the tabs for each of the product types
- Append all new generated HTML at the bottom of the page to create the Additional Products User Experience

### Categorized Search Results

Like the Additional Product Tabs section, I also had to manipulate the search results page so that the product listings would be displayed in a sectioned manner according to the product type they belong to. WSM does not natively display products this way, as it just lumps all matching products into one general container.

In order to achieve this UX, I had to rely on DOM manipulation again. In short, I had to create the containers for each product type, loop through each product and identify its product type, insert each product into its corresponding div container, and display the parent container in the proper order, where Bedslide's should always be the first result.
