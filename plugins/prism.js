import Prism from 'prismjs'
import "prismjs/themes/prism-tomorrow.css";
import "prismjs/plugins/unescaped-markup/prism-unescaped-markup.css";
import "prismjs/plugins/unescaped-markup/prism-unescaped-markup.min.js";
import "prismjs/plugins/normalize-whitespace/prism-normalize-whitespace.min.js";
export default Prism
