---
title: About
subtitle: A brief Summary
image: /images/about/me.jpg
---

Hey! My Name is Christian Ferrer. I've been designing and building websites for a living for around 8 years or so. Ever since my first experience using the internet and viewing websites, I've grown a passion for web design, HTML, and CSS.

Other than doing web stuff, I like to nerd out on PC tech and Camera Tech and just hangout with my family. I'm a relatively new parent so life has been a bit hectic, but its been great so far!

My journey into web started at a pretty young age. If you'd like to know more about my personal story into web, feel free to read more below.