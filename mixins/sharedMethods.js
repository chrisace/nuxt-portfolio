export const observer = {
  methods: {
    observer(el) {
      const options = {
        //spepcify scroller, uses viewport if omitted
        // root: document.querySelector('.scrollingDiv'),
        // optional margin offset
        rootMargin: "100px",
        //threshold when you'd like it to alert you.
        // off, half, and totally on
        threshold: [0, 0.25, 0.5, 1.0]
      };

      // const callback = (entries, observer) => {
      const callback = (entries, observer) => {
        // Entries are elements that are within view
        entries.forEach(entry => {
          // console.log(entry)
          if (entry.isIntersecting && entry.intersectionRatio >= 0.5) {
            if (entry.target.classList.contains("lazy")) {
              const imgSrc = entry.target.dataset.src;
              entry.target.setAttribute("src", imgSrc);
            }
            entry.target.classList.add("visible"); // css will animate in
            // console.log('im in')
            observer.unobserve(entry.target);
          } else {
            // entry.target.classList.remove('visible') //animate out
            // console.log('im out')
          }
          //at this point you can also un-observe for single animation
          // observer.unobserve(entry.target)
        });
      };

      const observer = new IntersectionObserver(callback, options);

      //get some elements to observe
      const boxes = document.querySelectorAll(...el);
      //observe each of those elements
      boxes.forEach(box => observer.observe(box));
    }
  },
  mounted() {
    this.observer(this.observe);
  }
};

export const scrollBarColor = {
  methods: {
    scrollBarColor() {
      const cfBody = document.querySelector("body");
      cfBody.setAttribute("style", `--scrollBarBtn:${this.primaryColor};`);
    }
  },
  mounted() {
    this.scrollBarColor();
    // console.log("hello" + this.primaryColor);
  }
};
