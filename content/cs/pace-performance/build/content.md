---
title: Site Build
tech:
  - HTML
  - CSS
  - Smarty
  - jQuery
  - Bootstrap
---

The build for this was challenging, but very involved. Being a part of the Web Shop Manager CMS, I did not have complete control over the whole site, so I had to code as best as I could within the CMS. There were also new features added to Web Shop Manager that were not well-documented, so it was a struggle to get some items to work properly

### WSM Templates

With these eCommerce sites, I usually start with setting up the templates properly. In WSM, templates house all the logic and widgets that will be used to display the sections of the site. There were typically around 5-10 templates per build depending on the project. These include:

- Variables Template
- Head Template
- Header Template
- Body Template
- Footer Template
- Scripts Template

Ultimately, my job here was to make sure all the templates were setup properly before doing any CSS. This includes accounting for any HTML and Smarty logic (PHP based templating language) was in place.

Most of the actual website content would flow through the Body template, where I would not have much control over how they're output. The Header and Footer templates are just that -- templates to setup the header and footer of the site. The Head template contained all the necessary scripts, widgets, meta tags, etc that would be found in a normal element. The Variables template housed all of the specific Smarty Variables that could be used throughout the site. For example, store hours, social media profiles, and other general items could be stored as variales in this template.

### CSS

Once all the templating was in place, I would then start on the CSS of the site. Using the approved Web Designs, I would start styling the pages from top to bottom. This was done in Google Chrome dev tools as WSM does not have the ability to be run locally. This meant no SCSS and I had to manually save my CSS styles into WSM. This was definitely a dated way of writing CSS, but it worked, and it was effective for sites built in this platform.

### Struggles

One of my struggles on this site was getting WSM's new search functionality to work properly. The widgets were in the proper places of the template, but none of the data would populate. In order to remedy this, I had to work with out backend team for more specific documentation on how to implement the search features properly.

The conversations resulted well. Because of the conversations, the team was provided with better documentation and more configuration options found in the back-end of the site for easier implementation. I learned that rather than hitting my head against the wall, sometimes its beneficial for the whole team to simply ask for help. This also helped the Cusomter Service team as they would no longer need to rely on a dev to fix simple issues. It was a win-win-win!
