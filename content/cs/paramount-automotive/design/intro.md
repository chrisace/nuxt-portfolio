---
title: A New Experience
tech:
  - Photoshop
  - Moqups
---

Paramount had a general vision for what they wanted, and it was my job to help them bring that vision to life. Long story short, they wanted to provide the aggressive aesthetic for their target audience of younger jeep and truck enthusiasts. In addition to this aesthetic, they wanted the user/shopping experience to be streamlined and targeted towards their audience. Lastly, they wanted to able to connect with their audience somehow.
