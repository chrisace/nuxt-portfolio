---
name: Work
subtitle: Here are some pieces I've worked on throughout my career.
image: /images/work/bedslide@2x.jpg
type: Automotive eCommerce
pUrl: /work/bedslide
url: https://bedslide.com/
title: Bedslide
---

Most of these sites were built at my previous employers platform, which mostly consisted of automotive eCommerce. For personal sites and side projects, I like to experiment with vanilla HTML or JAMStack sites using static site generators.