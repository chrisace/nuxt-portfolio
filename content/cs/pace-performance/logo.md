---
title: Pace's Logo
image: /images/work/case-studies/pace-performance/logos.jpg
tech:
  - Illustrator
---

Pace's logo at the time definitely needed a refresh. Although they had a distinguisehd brand, the logo did not read well on the website, and also did not scale well in print applications when resized.

To reconcile the logo, I knew I had to simplify it by removing unecessary elements. The first thing necessary change was to remove the text from being enclosed in the oval shape. This alone opened up the logo significantly.

Next, I removed the yellow "pace 1" as well as the url at the bottom. These were unecessary elements that were detracting from the important part of the logo, which was the "Pace Performance" brand name.

From removing those elements, the logo was already more legible and easier on the eyes. The only thing left was to update the styling of the "Pace Performance" text a bit by changing the colors and adding a little bit of motion with the extended section of the "P" in "Pace".
