---
title: Paramount Automotive
projectUrl: https://paramount-automotive.com/
logoImage: /images/work/case-studies/paramount-automotive/new-logo-white.png
sourceUrl: null
logo: false
wireframes: true
styleGuide: true
graphics: true
siteDesign: true
siteBuild: true
casePrimaryColor: "#990000"
---

Paramount Automotive was a relatively new company that catered to truck and jeep enthusiasts. The goal of this project was to provide an easy-to-use shopping experience while providing the aggressive site design aesthetic that would cater to more of their younger audience.
