export const caseStudyScript = {
  head() {
    return {
      title: "Case Study of " + this.generalInfo.projectName,
      meta: [
        {
          name: "description",
          content: "A look at the work done for " + this.generalInfo.projectName
        }
      ]
    };
  },
  transition: {
    name: "layout"
  },
  computed: {
    casePrimaryColor() {
      return {
        "--casePrimaryColor": this.intro.casePrimaryColor
      };
    },
    generalInfo() {
      return {
        projectName: this.intro.title,
        logo: this.intro.logoImage,
        projectUrl: this.intro.projectUrl,
        sourceUrl: this.intro.sourceUrl ? this.intro.sourceUrl : undefined
      };
    }
  },
  methods: {
    observer() {
      const options = {
        rootMargin: "100px",
        threshold: [0, 0.25, 0.5, 1.0]
      };
      const callback = (entries, observer) => {
        entries.forEach(entry => {
          if (entry.isIntersecting && entry.intersectionRatio >= 0.5) {
            const imgSrc = entry.target.dataset.src;
            entry.target.setAttribute("src", imgSrc);
            entry.target.classList.add("visible");
            observer.unobserve(entry.target);
          } else {
            // console.log('im out')
          }
        });
      };
      const observer = new IntersectionObserver(callback, options);
      const lazyImages = document.querySelectorAll(".work__image");
      lazyImages.forEach(lazyImage => observer.observe(lazyImage));
    },
    scrollBarColor() {
      const cfBody = document.querySelector("body");
      cfBody.setAttribute(
        "style",
        `--scrollBarBtn:${this.intro.casePrimaryColor};`
      );
    }
  },
  mounted() {
    this.observer();
    this.scrollBarColor();
  }
};
