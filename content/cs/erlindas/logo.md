---
title: The Logo
image: /images/work/case-studies/erlindas/logos.jpg
tech:
  - Freehand
  - Illustrator
---

Erlinda's wanted a simple, yet vibrant logo. They also wanted it to read well and have the text "Filipino cuisine & Ice Cream Parlor". Lastly if possible, they also wanted to represent the Filipino culture somehow and incorporate the colors green and yellow.

With those requirements, after many attempts, I ended up with the finished product displayed here. I incorporated a brighter green and yellow theme to provide that vibrancy they were referring to. Additionally, to add some Filipino flare, I figured I could incorporate the Filipino Sun as a graphical element somehow. In the end I ended up placing this graphical element around the "e", which also adds some vibrancy and gives the logo a more dynamic feel.

**Needless to say, they loved it!**
